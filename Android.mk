
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := init.dhcpcd
LOCAL_SRC_FILES := AndroidFS/system/vendor/bin/init.dhcpcd
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := init.config.ip
LOCAL_SRC_FILES := AndroidFS/system/vendor/bin/init.config.ip
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := swihltrace
LOCAL_SRC_FILES := AndroidFS/system/vendor/bin/swihltrace
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := slqssdk
LOCAL_SRC_FILES := AndroidFS/system/vendor/bin/slqssdk
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := SierraDMLog
LOCAL_SRC_FILES := AndroidFS/system/vendor/bin/SierraDMLog
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := SierraFwDl7xxx
LOCAL_SRC_FILES := AndroidFS/system/vendor/bin/SierraFwDl7xxx
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := SierraImgMgr
LOCAL_SRC_FILES := AndroidFS/system/vendor/bin/SierraImgMgr
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := SierraSARTool
LOCAL_SRC_FILES := AndroidFS/system/vendor/bin/SierraSARTool
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := DownloadTool
LOCAL_SRC_FILES := AndroidFS/system/vendor/bin/DownloadTool
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libsierra-ril.so
LOCAL_SRC_FILES := AndroidFS/system/vendor/lib64/libsierra-ril.so
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_TARGET_ARCH := arm64
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libsierraat-ril.so
LOCAL_SRC_FILES := AndroidFS/system/vendor/lib64/libsierraat-ril.so
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_TARGET_ARCH := arm64
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libsierrahl-ril.so
LOCAL_SRC_FILES := AndroidFS/system/vendor/lib64/libsierrahl-ril.so
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_TARGET_ARCH := arm64
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libswims.so
LOCAL_SRC_FILES := AndroidFS/system/vendor/lib64/libswims.so
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_TARGET_ARCH := arm64
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libswiqmiapi.so
LOCAL_SRC_FILES := AndroidFS/system/vendor/lib64/libswiqmiapi.so
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_TARGET_ARCH := arm64
include $(BUILD_PREBUILT)


include $(CLEAR_VARS)
LOCAL_MODULE := libDownloadTool.so
LOCAL_SRC_FILES := AndroidFS/system/vendor/lib64/libDownloadTool.so
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_TARGET_ARCH := arm64
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := sierra_logs_app
LOCAL_SRC_FILES := AndroidFS/system/app/com.sierra.logs/com.sierra.logs.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_VENDOR_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := swifota_app
LOCAL_SRC_FILES := AndroidFS/system/app/swifota/swifota.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_VENDOR_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := swiomadm_app
LOCAL_SRC_FILES := AndroidFS/system/app/swiomadm/swiomadm.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_VENDOR_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := simswitcher_app
LOCAL_SRC_FILES := AndroidFS/system/app/simswitcher/simswitcher.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_VENDOR_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := gps.$(TARGET_BOARD_PLATFORM).so
LOCAL_SRC_FILES := AndroidFS/system/vendor/lib64/hw/gps.default.so
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_TARGET_ARCH := arm64
LOCAL_MODULE_RELATIVE_PATH := hw
include $(BUILD_PREBUILT)